\section{Simulating Pac-Man in Act-R}
\label{pacman}
The goal is to recreate the experiment as described by \cite{Syriani2008} in act-r as well as in soar. Hopefully this gives some insight into the usability of both these tools for game development. First, the process in act-r is described, followed by the implementation in soar. As stated previously the python implementation of act-r will be used to implement the pac-man game. Once finished, playability tests will be performed. 	 
\subsection{The PacMan Language}
The model for simulating PacMan consists of 2 agents, PacMan and Ghost. Fortunately, a 2D-grid based environment is already present in python act-r. GridNode is just a subclass of already existing code that is based on a model. Score and Pellet are just parameters of this model, they are not represented by individual entities.
\subsection{Semantics}
Each agent in a model contains a set of rules \footnote{The full set of rules and the environment can be found in Appendix 1}. These rules specify the behaviour of the Agent. On top of this, each agent contains a goal-buffer, called focus. Different rules fire depending on the contents of this buffer. An example of a rule:
\lstset{language=Python,caption={Example rule from the PacMan agent},label=pacrule}
\begin{lstlisting}
def look_for_pellet(focus='looking',vision='busy:False',visual=None):
        vision.request('pellet:True x:?x y:?y')
        focus.set('found')
\end{lstlisting}
This rule fires if the focus is looking for a pellet. If this is so, this rule fires, and requests from the vision system the location of a pellet. The focus is changed and a new rule will fire. It is important to note that in act-r rules are fired sequentially. If two rules happen to match the same buffer, one is selected randomly from the matches. A system is in place to give preference to rules that are called more often, but in this model this has no effect and all rules have the same chance of being selected.
Just as in the aforementioned paper, the focus will be on playability, so score will be ignored.
\subsection{The PacMan case study}
There are two main agents in the PacMan model, PacMan and Ghost. Both have a Motormodule, a visual memory and a module to detect obstacles; PacMan wanders around, asks is visual system for the location of a pellet and directly heads to the location reported while trying to evade the Ghost. The Ghost requests the location of PacMan and heads towards the reported result, updating its course if PacMan moves.	The motormodule is just a high level representation of movement. It is an abstraction of moving in different directions. On top of this the obstaclemodule just makes sure that PacMan or Ghost can not move through walls.

PacMan selects a pellet based on a property called salience. Salience indicates how much an item stands out in a collection \citep{Stewart2007}. An illustration: in a row with blue balls, a red ball has a high salience. Standard, chunks with a high salience have a high preference. Since there is only one node with a ghost present, this will be chosen every time. Obviously this is not the desired behaviour, on the contrary, in this case PacMan would move straight to the Ghost every single time. With some workarounds it is possible to use a user-defined function to calculate salience in stead of the built-in salience mechanism. Now, salience is calculated by taking into account the distance to a square and the presence of a ghost in the neighbourhood. If different squares have identical salience, one is randomly chosen. Ghost just spots PacMan, and approaches using the manhattan distance.

This is the method used to model the \emph{smart player}, modelling the random and dummy player is perfectly possible as well, by restricting the view of PacMan to only adjacent nodes or by just creating a move rule for every direction, such that each one of these rules has the same chance of firing. Given the fact that only the smart user is discussed in the simulations, this is not further discussed.

\subsubsection{Use of time}
We need to make sure that we do not change the semantics as described in \citep{Syriani2008}. In that simulation, every transition happened instantaneously except when making a decision. This parameter can be varied for simulation purposes. We will do the same by waiting a specified amount of time when making a decision:
\begin{lstlisting}
def look_for_pellet(focus='looking',vision='busy:False',visual=None):
        vision.request('pellet:True x:?x y:?y')
        focus.set('wait')
        
def wait(focus='wait'):
        import time
        begin = time.time()
        
        while(time.time() - begin < pac_production_time):
            pass

        focus.set('found') 
\end{lstlisting}

Requesting the location of PacMan corresponds to the moment in time where you make a decision. After this the production process continues as normal.

\subsection{Experiment} 
Player decision time is taken from the same distribution as in \cite{Syriani2008}, with the exact same parameters:
\[
F(x) = e^{-e^{\frac{b - x}{a}}}
\]

where a slow user has a =33.3 b = 284, a normal user a =19.9 and b = 257, a fast user a =28.4 and b = 237 and a VeryFast user with a =17.7 and b = 222. We sample from this distribution using the Inverse Cumulative Method.

Only the smart movement strategy is considered and the length of the simulated game is measured on a board consisting of 22 food pellets. Ghost decision time was varied from 100ms to 400 ms, but due to timing constraints, the average time of a game is quite a bit longer than in the original results, an average over 50 samples was taken instead of 100 samples.
\begin{figure}[t!]
				\centerline{\includegraphics[width=500px]{avg_durations}}
				\caption{Time till end}
				\label{Fig:avg_durations_actr}
			\end{figure}
The results are shown in \ref{Fig:avg_durations_actr}. A reminder: speed here is the time it takes for a Ghost to make a decision. On first sight our results are comparable with the results from the original experiment, except the fact that games last 2.5 times longer for slower reaction times. Despite this, the same idea is valid, the slope of the curve implies that the slower the decision time, the longer the game lasts and after a certain limit, we get to see the same plateau, but this occurs 100ms sooner, at 250 ms. An explanation for this is that PacMan does not follow an optimal strategy and, more importantly, by letting each rule take quite a lot of time, games will last longer automatically. This is inherent to the way act-r works and how this model was made.
\begin{figure}[t!]
				\centerline{\includegraphics[width=500px]{victory_freq}}
				\caption{Victory Frequency}
				\label{Fig:victory_freq_actr}
			\end{figure}
			
\ref{Fig:victory_freq_actr} depicts the frequency with which a player will win a game as function of the time spent on the Ghost's decision. A result of 75 \% is considered a good amount of wins to keep a game interesting, yet challenging enough. The graph shows that this model does not even reach this percentage. The best result is a 50\% win rate. Obviously this is not enough. The main cause for this is the fact that the salience system used for pathfinding is not adequate enough. If the game is run in visual mode, so that the progress of the game is visible, it is clear that the model of a human player still shows some quirks and that, once in a while, stupid moves are made. This reduces the win rate drastically. Even raising the Ghost Decision time has no remarkable effect.

\begin{figure}[t]
	\centerline{\includegraphics[width=450px, height=250px]{act_r_pacman}}
				\caption{The ACT-R Pacman environment. The yellow arrow represents PacMan, while the purple arrow represents a Ghost. If a square is green, it means there is still a pellet present}
				\label{Fig:Pacman_env}
\end{figure}

\subsection{Conclusion}
It is definitely possible to recreate the experiment and develop a basic AI in act-r. The results were not as good as in the original experiment, but the model is to blame for this, not act-r. If the model would be further refined, I am certain these problems can be solved. This is in itself the biggest shortcoming of this tool: the lack of examples and documentation. There are some examples available, but these only show a basic use of act-r, making a complex model is an art in itself. A lot of papers can be found, describing different solutions for various problems, but these are only descriptions, the code is, more often than not, not available. In the short timespan of this research it was not possible to contact authors and developers requesting more information.

Concerning the explicit use of time, a better solution may exist. As mentioned before in \ref{act_unreal}, an unreal tournament bot has been implemented using Act-R and sockets, where the rules keep on firing and at certain intervals the act-r system is polled and the necessary information is passed along, though it was not feasible implementing this for a project of this scope. If act-r would be used in a bigger project, this will probably work better than the method used in this experiment, even though, again, documentation is severely lacking. 

