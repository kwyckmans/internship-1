pac_production_time=0.2
ghost_production_time=0.4
user="normal"
'''
Created on 30-jul.-2012

@author: Kevin
'''
import ccm
import itertools
log = ccm.log(data = True)

from ccm.lib import grid
from ccm.lib.actr import *
from ui import swi
from ccm.lib import cellular
map = """
########
# ppppp#
#pppppp#
#pppppp#
#ppppp #
########
"""    

#=============================================
# map = """
# ####
# #pp#
# #pp#
# ####
# """
#=============================================

class MyCell(grid.Cell):
    '''
    classdocs
    '''
    pellet = False
    def color(self):
        if self.wall: return 'black'
        elif self.pellet: return 'green'
        else: return 'white'
        
    def load(self, char):
        if char =='#': self.wall = True
        elif char =='p': self.pellet = True
        
class GhostBody(grid.Body):
    def _list_visible_objects(self):
        for row in self.world.grid:
            for cell in row:
                if not cell.wall is True:
                #if cell.pellet is True:
                    if self.cell != cell:
                        yield cell
        
        for obj in self.world.agents:
            yield obj
    
class PacManWorld(grid.Body):
    def _list_visible_objects(self):
        for row in self.world.grid:
            for cell in row:
                if not cell.wall is True:
                #if cell.pellet is True:
                    if self.cell != cell:
                        yield cell
        
        for obj in self.world.agents:
            yield obj
            
    def go_towards(self,target,y=None):
        for obj in world.agents:
            if obj.color is "purple":
                ghostx = obj.cell.x
                ghosty = obj.cell.y
               # print "Location of ghost is", ghostx, ghosty
        bestDist = 0
        if not isinstance(target,cellular.Cell):
            target=self.world.grid[int(y)][int(target)]
        
        if self.world is None: raise cellular.CellularException('Agent has not been put in a World')
        if self.cell==target: return
        best=None
        for i,n in enumerate(self.cell.neighbours):
            if n==target:
                best=target
                bestDir=i
                break
            if getattr(n,'wall',False): continue
            dist=(n.x-target.x)**2+(n.y-target.y)**2
            if (best is None or bestDist>dist):
                if not (n.x == ghostx and n.y == ghosty):
                    best=n
                    bestDist=dist
                    bestDir=i
        if best!=None:
            if getattr(best,'wall',False): return False
            self.cell=best
            self.dir=bestDir
            return True
            
class GhostWorld(grid.Body):
    pass
            
class PacVisionScanner(grid.VisionScanner, MemorySubModule):
    def activation(self, chunk):
       # print "Returning activation", chunk["salience"]
        return chunk['salience']
    
    def start(self):
        
        while True:
            for obj in self._body._list_visible_objects():
                if hasattr(obj,'cell'):
                    if getattr(obj,'x',None)!=obj.cell.x: obj.x=obj.cell.x
                    if getattr(obj,'y',None)!=obj.cell.y: obj.y=obj.cell.y
                try:
                    pellet = obj.cell.pellet
                    ox,oy=obj.x,obj.y
                    x,y=self._body.x,self._body.y
                    salience=self.salience(ox-x,oy-y, ox, oy, pellet)
                    obj.salience = salience
                except AttributeError:
                    salience=1
                    obj.salience = salience
            
            #Items are added to the memory here
                self._visual.add(obj)
                
            yield self.scan_time
        
    def salience(self,dx,dy, ox, oy, *pellet):
        #print "Calculating Salience"
        import math
        for obj in self._body.world.agents:
            if obj.color is "purple":
                ghostx = obj.cell.x
                ghosty = obj.cell.y
        
        ghostx = 5
        ghosty = 5
        #print self._body.x
        dgx = ghostx - ox
        dgy = ghosty - oy
        
        dist=dx*dx+dy*dy
        dist_g = dgx*dgx + dgy * dgy
        
        s_d=  2/(math.sqrt(dist)+1)  
        s_g = (math.sqrt(dist_g) +2)/5
        
        s = s_d + s_g
        #if pellet: s += 1
        #if s>1: s=1
        return s 
    
class GhostVisionScanner(grid.VisionScanner):
    def salience(self,dx,dy):
        return 1
    
class GhostMotorModule(ccm.Model):
    busy=False
    
    def go_towards(self,x,y):
        if self.busy: return
        self.busy=True
       # print 'Ghost going towards %s %s'%(x,y)
        self.action='going towards %s %s'%(x,y)
        #yield 0.01#self.parent.production_time
        self.parent.body.go_towards(x,y)
        self.action=None
        self.busy=False

          
class PacMotorModule(ccm.Model):
    pellets = 22
    busy=False
    
    def go_towards(self,x,y):
        if self.busy: return
        self.busy=True
       # print 'going towards %s %s'%(x,y)
        self.action='going towards %s %s'%(x,y)
        #yield 0.01# self.parent.production_time
        if self.parent.body.cell.pellet == True: self.eat()
        self.parent.body.go_towards(x,y)
        self.action=None
        self.busy=False
        
    def eat(self):
        if self.parent.body.cell.pellet:
            self.parent.body.cell.pellet=False
            self.pellets = self.pellets - 1
            if self.pellets == 0:
                log.win = 1
            #  print "pellets left:", self.pellets
          
class ObstacleModule(ccm.ProductionSystem):
    production_time=0
    ahead=False
    left=False
    right=False
    def check_ahead(self='ahead:False',body='ahead_cell.wall:True'):
        self.ahead=True
    def check_left(self='left:False',body='left_cell.wall:True'):
        self.left=True
    def check_right(self='right:False',body='right_cell.wall:True'):
        self.right=True

    def check_ahead2(self='ahead:True',body='ahead_cell.wall:False'):
        self.ahead=False
    def check_left2(self='left:True',body='left_cell.wall:False'):
        self.left=False
    def check_right2(self='right:True',body='right_cell.wall:False'):
        self.right=False
        
class Ghost(ACTR):
    ghost_production_time = ghost_production_time    
    waitime = 0
    focus = Buffer()
    body = GhostBody()
    motor = GhostMotorModule()
    obstacle=ObstacleModule()
    
    visual=Buffer()
    vision=Memory(visual)
    
    visionScanner = GhostVisionScanner(body,vision)
    
    def init():
        focus.set('wander')
        visual.clear()
        
    def look_for_pacman(focus='wander', vision='busy:False', visual=None):
        vision.request('color:yellow')
        focus.set('wait')
        #time.sleep(ghost_production_time)
    
    def wait(focus='wait'):
        import time
        begin = time.time()
        
        while(time.time() - begin < ghost_production_time):
            pass

        focus.set('found') 
      
    def pacman_found(focus='found', visual='color:yellow x:?x y:?y'):
        waittime = 0
        focus.set('go towards ?x ?y')
     
    def going_to_pacman(focus = 'go towards ?x ?y', motor='busy:False'):
        if (self.body.x == x) and (self.body.y == y):
            self.stop()
        motor.go_towards(x, y)
        visual.clear()
        vision.clear()
        
    def arrived(focus = 'go towards ?x ?y', body='x:?x y:?y'):
        vision.request('color:yellow')
      
    def checking_location(focus = 'go towards ?x ?y', vision="busy:False", visual = None):
        vision.request('color:yellow')
        if (self.body.x == x) and (self.body.y == y):
            self.stop()
          
    def updating_location(focus = 'go towards ?x ?y', visual ='color:yellow x:?dx y:?dy'):        
        for obj in self.body._list_visible_objects():
            if obj.color is "yellow":
                pacx = obj.cell.x
                pacy = obj.cell.y      
              
                if (pacx == self.body.x) and (pacy == self.body.y):
                    focus.set('lost')
                    break
        else:
            if (x != dx) or (y != dy):
                motor.go_towards(dx, dy)
                focus.set('go towards ?dx ?dy')
              
                visual.clear()
                vision.clear()
            else:
                focus.set('go towards ?x ?y')
                
    def finished(focus = 'lost'):
        self.stop()
            
    
class MyAgent(ACTR):
    waittime = 0
    pac_production_time = pac_production_time
    focus=Buffer()
    body= PacManWorld()
    motor = PacMotorModule()
    obstacle = ObstacleModule()
    
    visual=Buffer()
    vision=Memory(visual)

    visionScanner=PacVisionScanner(body,vision)
    vision.add_adaptor(visionScanner)
    
    def init():
        focus.set('looking')
        visual.clear()
       # salience.context()
                   
    def look_for_pellet(focus='looking',vision='busy:False',visual=None):

        vision.request('pellet:True x:?x y:?y')
        focus.set('wait')
        
    def wait(focus='wait'):
        import time
        begin = time.time()
        
        while(time.time() - begin < pac_production_time):
            pass

        focus.set('found') 
                   
    def found_pellet(focus='found',visual='pellet:True x:?x y:?y'):
       # vision.request('color:purple')
        focus.set('go to ?x ?y')
        #visionScanner.salience(x, y)
        #vision.clear()
        
    def go_to_location(focus='go to ?x ?y',motor='busy:False'):
        import math
        visual.clear()
        vision.clear()
        for obj in self.body._list_visible_objects():
            if obj.color is "purple":
                pacx = obj.cell.x
                pacy = obj.cell.y      
              
                if (pacx == self.body.x) and (pacy == self.body.y): 
                    focus.set('lost')
                    break
        else:
           #=========================================
           # # if(math.fabs(self.body.x - int(gx)) == 1) and (math.fabs(self.body.y - int(gy)) == 1):
           #     self.body.turn_around()
           #     focus.set("looking")
           # 
           # else:#
           #========================================
            motor.go_towards(x,y)
            focus.set('go to ?x ?y')
        
        
    def arrived(focus='go to ?x ?y',body='x:?x y:?y'):
        for obj in self.body._list_visible_objects():
            if obj.color is "purple":
                pacx = obj.cell.x
                pacy = obj.cell.y      
                
                if (pacx == self.body.x) and (pacy == self.body.y):
                    focus.set('lost')
                else:
                    motor.eat()
                    vision.clear()
                    visual.clear()
                    focus.set('looking')
    
    #===============================================
    # def checking_location(focus = 'go to ?x ?y ?gx ?gy', vision="busy:False", visual = None):
    #    vision.request('color:purple')
    #        
    # def updating_location(focus = 'go to ?x ?y ?gx ?gy', visual = 'color:purple x:?dx y:?dy'):
    #        
    #    if (gx != dx) or (gy != dy):
    #        print "UPDATING"
    #        focus.set('looking')
    #        visual.clear()
    #        vision.clear()
    #=============================================
    
    def lost(focus='lost'):
        self.stop()
        
    def finished(motor='pellets:0'):
        self.stop()
        
world=grid.World(MyCell,map=map, directions = 4)

pacman = MyAgent()
pacman.body.color='yellow'
world.add(pacman,x=1, y=1, dir=2)

ghost = Ghost()
ghost.body.color = "purple"
world.add(ghost, x=6, y=4, dir=0)

#ccm.log_everything(pacman,log.yellow)
#ccm.log_everything(ghost, log.purple)

#display = ccm.display(world)
world.run()
ccm.finished()
