\section{A Pac-Man Casy Study}
After providing an explanation about the internals of SOAR and ACT-R, a small case-study is provided as an example of how to use these tools in a game-development context. We will base our implementation on \citep{Syriani2008}, in which graph-rewriting is used to implement and simulate a basic version of the game Pac-Man. We will use the language and semantics used in this paper, so that results can be compared. In this section we will describe the structure and semantics of the game, in the following sections the actual implementation and simulation will be discussed.

\subsection{The Pac-Man Language and Semantics}
We can distinguish five different elements in Pac-Man: Pac-Man, the ghost, food (called pellets), GridNode and ScoreBoard. For our version, we only need to consider Pac-Man and Ghost as important entities, the other three elements are just part of the game-framework. Pac-Man and Ghost will be represented by different agents, controlled by the player or the ACT-R/SOAR system.

The operational semantics are defined by the production rules that are the core of all cognitive modelling tools. Rules will exist to move Pac-Man, to flee from a ghost, to eat food. Priorities can be given to rules to control decision flow and eventually to make an agent smarter (priorities in ACT-R and preferences in SOAR). 

\subsection{Using Cognitive Modelling tools to simulate behaviour}
The basic idea is that we write a framework providing the basic Pac-Man game and the necessary functions to efficiently perform some analysis on. Then we can plug-in SOAR, ACT-R, or any other tool for that matter, providing the intelligence behind the ghost or pac-man. We can then let it run once, visual if possible, or multiple times so that we can draw conclusions about our system or that we can test the playability of our game.

Each pass of the game-loop the current state of the game is passed on to whatever system is chosen, which in turn returns the necessary commands to move pac-man and/or the ghost (cfr. the Motor System in ACT-R and SOAR).

\subsection{Creating a playable game model}
If we want to create a game that is actually playable we have to take into account two factors:
\begin{itemize}
	\item Firing rules in ACT-R and SOAR takes a specific amount of time (50 ms). All of the systems besides the production system use time as well, to represent delays in human thinking. This is not necessarily applicable to all game-agents and all situations in a real time game.
	\item You need to take into account playability issues, such as a ghost moving to fast in relation to a human player. Your ghost can not be too smart, or the game will not be fun to play at all.
\end{itemize}

\subsection{Modelling the player and the game}
We would like to be able to quantify the quality of the game. This means we should be able to let the game run automatically and thus we should create a model for our players. Concerning pac-man, there are two important factors to be taken into account: reaction speed and decision analysis (e.a. path finding). If we want to be able to evaluate playability, we need a performance metric, in this case the number of wins vs. losses. We should be able to win often enough, but not all the time. 

Different players may use different strategies, the following three will be used:
\begin{itemize}
	\item Random: PacMan will move randomly, inlcuding illegal moves.
	\item Dumb: PacMan will take into account its directly adjacent nodes, it moves to the adjacent node that has food on it, but not a ghost.
	\item Smart: PacMan recieves a global view of the playing field. You use pathfinding to compute the shortest path to the nearest food.
\end{itemize}

Implementing complex pathfinding in SOAR and ACT-R is not easy and a topic on itself. An easy solution is to just use the manhattan distance. Even better would be to use the internal memory and learning mechanisms of SOAR and ACT-R, to let them learn that ghosts are bad and pellets are good (and closer ones even better). It might be possible that a couple of runs are needed beforehand to tune the learning mechanisms before the actual simulation can be performed.
	
Exactly the same strategies can be used to describe different levels of intelligence in Ghosts, replacing food by Pac-Man.

\subsection{Explicit use of time}
Time is a critical aspect in this simulation since playability depends on the relative speed of the player (controlling PacMan) and game (Ghost). Our framework provides the game loop controlling the game, which is for all intents and purposes represents the continuous passing of time. The difficulties arise with the production system. In ACT-R each rule matches instantly, but takes 50ms to fire and top of this, when using complex modules from SOAR or ACT-R, time is added depending on which system is use, while in SOAR a decision cycle takes avout 50ms, although more rules can fire leading to the decision.

%We want to be able to vary these, so we can research the effect on the playability of our game, since these are the only parameters we can control. In ACT-R you can easily set the production time while initializing (and the time for each system on top of this when using it). 
%In SOAR this is not  \texttt{max-dc-time} parameter which limits the entire runtime for

We want to be able to tweak these parameters, since in real life payer's reaction speed differs from game to game, and even within one game. The game will run at speeds starting at 100ms to 400ms using a 5ms interval. Player speed will be determined by using the inverse cumulative method and the following distribution:
$$
F(x) = e^{-e^{\frac{b - x}{a}}}
$$
with a = 33.3 and b = 284 for a slow user, a = 19.9 and b = 257 for a Normal user, a Fast user has a = 28.4 and b = 237 and a VeryFast user fas a = 17.7 and b = 222. This value will be used to configure the speed in ACT-R and SOAR.

For each game-speed we will average 100 samples simulated with different seeds. The parameter we will use as a performance metric is the \emph{time until game ends}. We will also take note of the frequency with which a player will win a game. The goal is a win-percentage of 75\%.

\subsection{Extra information}
ACT-R and SOAR both have simple games as part of the examples in the suites you can download. In the previous section some implementations of bigger projects using these tools have been mentioned, they all have papers describing the implementation. While ACT-R has a python implementation and thus makes it easy to do computations and extend the system just using python, it is a bit more complex in SOAR, since it has its own specific syntax and can not easily be extended, thus some extra information about path finding in SOAR can be found in \citep{Hangartner94}, \url{http://web.eecs.umich.edu/~soar/sitemaker/workshop/23/Kerfoot%20-%20High%20Level%20AI.pdf} and \url{http://web.eecs.umich.edu/~soar/sitemaker/workshop/21/Laird-Learning.pdf}.
