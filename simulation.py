import ccm
import math
import random
slowa=33.3
slowb=284
normala=19.9
normalb=257
fasta=28.4
fastb=237
vfasta=17.7
vfastb=222

#Test for the slow user
for i in range(0,100):
    print "run", i + 1, 'from', 50
    for x in range(100,400,10):
        u = random.uniform(0, 1)
        pac_speed = -(math.log(-math.log(u))*slowa) + slowb
        ccm.run('pacman',1, pac_production_time=pac_speed*.001,ghost_production_time=x*.001, user='slow' )


#Test for the normal user
for i in range(0,50):
   print "run", i + 1, 'from', 50
   for x in range(100,400,10):
       u = random.uniform(0, 1)
       pac_speed = -(math.log(-math.log(u))*normala) + normalb
       ccm.run('pacman',1, pac_production_time=pac_speed*.001,ghost_production_time=x*.001, user='normal' )

#Test for the fast user
for i in range(0,50):
   print "run", i + 1, 'from', 50
   for x in range(100,400,10):
       u = random.uniform(0, 1)
       pac_speed = -(math.log(-math.log(u))*fasta) + fastb
       ccm.run('pacman',1, pac_production_time=pac_speed*.001,ghost_production_time=x*.001, user='fast' )
       

#Test for the very fast user

for i in range(0,100):
   print "run", i + 1, 'from', 50
   for x in range(100,400,10):
       u = random.uniform(0, 1)
       pac_speed = -(math.log(-math.log(u))*vfasta) + vfastb
       ccm.run('pacman',1, pac_production_time=pac_speed*.001,ghost_production_time=x*.001, user='vfast' )

